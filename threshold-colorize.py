#!/usr/bin/env python2.7
from gimpfu import *

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)


class Logger:

    def __init__(self):
        pass

    @staticmethod
    def status_message(message):
        pdb.gimp_message(message)


def threshold_layers(
        image,
        drawable,
        n_layers=1,
):
    denominator = float(n_layers + 1)
    # logger = Logger()

    for i in range(n_layers):
        lower_threshold = (n_layers - i) / denominator
        pdb.gimp_selection_none(image)
        layer = drawable.copy()
        image.add_layer(layer)
        pdb.gimp_layer_add_alpha(layer)
        layer.name = 'threshold_' + str(i + 1)
        pdb.gimp_drawable_threshold(layer, 0, lower_threshold, 1)
    # return original context parameters
    pdb.gimp_selection_none(image)


description = 'Use selected layer to produce several new layers from colorized threshold operations'

register(
    'webercode_threshold_layers',
    description,
    description,
    'Troy Weber',
    'WeberCode',
    '2020',
    '<Image>/Filters/WeberCode/Threshold Layers',
    'RGB*, GRAY*',
    [
        (PF_INT, 'n_layers', 'Number of Layers', 1)
    ],
    [],
    threshold_layers
)

main()
